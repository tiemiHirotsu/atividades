<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Empresa</title>
</head>
<body>
    <ul>
        <li><a href="{{route('home')}}">Home</a></li>
        <li><a href="{{route('empresa')}}">Empresa</a></li>
        <li><a href="{{route('servico')}}">Serviços</a></li>
        <li><a href="{{route('contato')}}">Contato</a></li>
    </ul>

    <h1>Empresa</h1>
    
    <h2>Missão</h2>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem nisi, ad facere delectus placeat omnis dolor ea tenetur aperiam laborum aut dolorum ut sint consequuntur, dicta exercitationem vitae ullam blanditiis?</p>

    <h2>Visão</h2>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem soluta asperiores harum repellendus maxime odit debitis possimus error assumenda doloremque ex id, minus iste earum exercitationem dolor incidunt, consequatur dolorem!</p>
      
</body>
</html>