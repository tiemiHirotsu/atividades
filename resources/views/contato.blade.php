<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contato</title>
</head>
<body>
    <ul>
        <li><a href="{{route('home')}}">Home</a></li>
        <li><a href="{{route('empresa')}}">Empresa</a></li>
        <li><a href="{{route('servico')}}">Serviços</a></li>
        <li><a href="{{route('contato')}}">Contato</a></li>
    </ul>

    <h1>Contato</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet corporis in nulla, culpa saepe inventore, dicta fugit porro molestiae unde ex harum! Fuga, in adipisci sit quaerat ad reprehenderit minima.</p>

    <form>
        Nome:<br>
        <input type="text" name="nome">
        <br>
        E-mail:<br>
        <input type="text" name="email">
        <br>
        Mensagem:<br>
        <textarea rows="4" cols="50" name="comment" form="usrform">
            Mensagem...</textarea>
      </form>
      
</body>
</html>